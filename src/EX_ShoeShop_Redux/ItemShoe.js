import React, { Component } from "react";
import { connect } from "react-redux";
import { BUY_SHOE, VIEW_DETAIL } from "./redux/constant/shoeConstant";
import { handleBuy, viewDetailAction } from "./redux/action/shoeAction";
class ItemShoe extends Component {
  render() {
    let { handleViewDetail, handleBuy } = this.props;
    let shoe = this.props.data;
    return (
      <div className="col-4 p-1">
        <div className="card text-left h-100 border-info">
          <img className="card-img-top " src={shoe.image} alt="Picture" />
          <div className="card-body">
            <h4 className="card-title text-center">{shoe.name}</h4>
          </div>
          <button
            onClick={() => {
              handleViewDetail(shoe);
            }}
            className="btn btn-info"
          >
            Show Detail
          </button>
          <button
            onClick={() => {
              handleBuy(shoe);
            }}
            className="btn btn-danger"
          >
            Add To Cart
          </button>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (dispatch) => {
  return {
    handleViewDetail: (shoe) => {
      dispatch(viewDetailAction(shoe));
      // before: dispatch action
      // after: dispatch function, function return action
    },
    handleBuy: (shoe) => {
      dispatch(handleBuy(shoe));
    },
  };
};
export default connect(null, mapStateToProps)(ItemShoe);
