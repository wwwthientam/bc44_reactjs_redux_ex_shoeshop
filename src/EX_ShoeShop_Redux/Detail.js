import React, { Component } from "react";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    let { name, price, description, quantity } = this.props.detail;
    return (
      <div>
        <h2>Detail Product</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">{name}</td>
              <td>{price}</td>
              <td>{description}</td>
              <td>{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detailShoe,
  };
};
export default connect(mapStateToProps)(Detail);
