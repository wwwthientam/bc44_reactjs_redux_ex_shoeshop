import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_AMOUNT, DELETE_SHOE } from "./redux/constant/shoeConstant";

class CartShoe extends Component {
  render() {
    let { cart, remove, handleChangeAmount } = this.props;
    return (
      <div className="col-6">
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td>{item.price * item.soLuong}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleChangeAmount(item, -1);
                      }}
                      className="btn btn-danger"
                    >
                      -
                    </button>
                    <strong className="mx-3">{item.soLuong}</strong>
                    <button
                      onClick={() => {
                        handleChangeAmount(item, 1);
                      }}
                      className="btn btn-danger"
                    >
                      +
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        remove(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      X
                    </button>
                  </td>
                  <td>
                    <img style={{ width: 50 }} src={item.image} alt="Picture" />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    remove: (idShoe) => {
      let action = {
        type: DELETE_SHOE,
        payload: idShoe,
      };
      dispatch(action);
    },
    handleChangeAmount: (shoe, option) => {
      let action = {
        type: CHANGE_AMOUNT,
        payload: {
          // long-type: shoe: shoe, option: option,
          shoe,
          option,
        },
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
